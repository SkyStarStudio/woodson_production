# coding=UTF-8
from django.test import TestCase, Client
from . import views, models
from . import forms
from django.contrib.auth.models import User


class CustomerTestCase(TestCase):
	"""
	测试用户注册,登录等方法
	"""
	def setUp(self):
		self.client = Client()
		# 创建一个 User, 用于测试激活链接
		self.test_user = User.objects.create_user('test', 'test@test.com', '123456')
		self.test_user.is_active = False

	def test_activate_flow(self):
		"""
		测试生成激活链接的方法: views.create_activation_key
		根据创建的User生成一套链接
		"""
		activation_key = views.create_activation_key(self.test_user)
		profile = models.Profile.objects.create(user=self.test_user, activation_key=activation_key)
		profile.save()
		result = views.activate_user(profile.activation_key)
		self.assertEqual(result['status'], 'success')
		self.assertEqual(result['user'], self.test_user)

	def test_register(self):
		"""
		测试注册的方法,使用 client 的方式
		"""
		data = {
			'username': 'test_register_user',
			'email': 'test_user@test.com',
			'password': '123456',
			'password_check': '123456',
		}
		response = self.client.post('/account/register/', data=data)

		user = User.objects.get(email=data['email'])
		self.assertEqual(user.username, data['email'])
		self.assertEqual(response.status_code, 302)

	def test_activate_user(self):
		"""
		测试激活用户的方法
		"""
		user = User.objects.create_user('test_activate_user', 'activate@test.com', '123456')
		user.is_active = False
		user.save()

		key = views.create_activation_key(user)
		profile = models.Profile.objects.create(user=user, activation_key=key)
		profile.save()

		url = '/account/activate/?key=%s' % key
		response = self.client.get(url)
		self.assertEqual(response.content, 'success')


	def test_register_form_mismatch_password(self):
		"""
		测试 RegisterForm 的 clean 方法: mismatch_password
		"""
		data = {
			'username': 'JasonTC',
			'email': 'jasontc@126.com',
			'password': '123456',
			'password_check': '123457',
		}
		r = forms.RegisterForm(data)

		self.assertEqual(r.is_valid(), False)

	def test_register_form_wrong_email_address(self):
		"""
		测试 RegisterForm 对于 Email 格式的验证
		"""
		data = {
			'username': 'JasonTC',
			'email': 'jasontc@126',
			'password': '123456',
			'password_check': '123456',
		}
		r = forms.RegisterForm(data)

		self.assertEqual(r.is_valid(), False)

	def test_login_user_with_exist(self):
		"""
		测试已经注册过的用户登录
		"""
		# 注册并激活一个 User 对象
		# 测试的时候需要注意, username 要与email一样
		user = User.objects.create_user('test_login@test.com', 'test_login@test.com', '123456')
		user.is_active = False
		user.save()

		key = views.create_activation_key(user)
		profile = models.Profile.objects.create(user=user, activation_key=key)
		profile.save()
		views.activate_user(key)

		data = {
			'email': 'test_login@test.com',
			'password': '123456'
		}
		response = self.client.post('/account/login/', data=data)
		self.assertEqual(response.status_code, 302)

	def test_login_user_with_non_exist(self):
		"""
		测试没有注册过的用户
		"""
		data = {
			'email': 'test_login_nonexist@test.com',
			'password': '123456',
		}
		response = self.client.post('/account/login/', data=data)
		self.assertEqual(response.content, 'User does not exist')

	def test_login_user_with_wrong_password(self):
		"""
		测试登录用户密码输入错误
		"""
		data = {
			'email': 'test_login@test.com',
			'password': '123456',
		}
		response = self.client.post('/account/login/', data=data)
		self.assertEqual(response.content, 'User does not exist')