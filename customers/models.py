# coding=UTF-8
from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
	"""
	扩展 django原生自带的 UserModel
	@user: OneToOneField的User对象
	@nickname: 即用户输入的username
	@activation_key: 激活链接
	@gender: 性别
	@birthday: 生日
	@phone: 电话
	这里没有保存CreditCard和 Address 等信息, 采用外键的形式保存,不在这张表里添加过多的外键.保证 User 逻辑简单.
	"""
	user = models.OneToOneField(User, unique=True)
	nickname = models.CharField(max_length=255, blank=True)
	activation_key = models.CharField(max_length=40, unique=True)
	gender = models.CharField(
		max_length=1,
		choices=(
			('M', 'Male'),
			('F', 'Female'),
		),
		null=True,
		blank=True
	)
	birthday = models.DateField(auto_now_add=False, auto_now=False, blank=True, null=True)
	phone = models.CharField(max_length=13, blank=True, null=True)
	# 增加一个标签用来表示用户是否使用过braintree进行支付,以便选择customer_id的使用
	braintree_customer = models.BooleanField(default=False)


ADDRESS_TYPE_CHOICES = (
	('SHIP', 'Shipment Address'),
	('BILL', 'Billing Address'),
)
class Address(models.Model):
	"""
	客户地址 Model
	@alias
	@full_name
	@street_address
	@zip
	@city
	@country
	@state
	@phone
	@email
	@type: (shipment, billing)
	@user: User
	"""
	alias = models.CharField(max_length=20, null=True, blank=True)
	full_name = models.CharField(max_length=40)
	street = models.CharField(max_length=255)
	zipcode = models.CharField(max_length=5)
	city = models.CharField(max_length=30)
	country = models.CharField(max_length=30)
	state = models.CharField(max_length=30)
	phone = models.CharField(max_length=15)
	address_type = models.CharField(max_length=4, choices=ADDRESS_TYPE_CHOICES)
	user = models.ForeignKey(User)

	def __str__(self):
		return '(' + self.alias + ') ' + self.full_name + ', ' + self.street