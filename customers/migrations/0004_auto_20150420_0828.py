# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0003_auto_20150419_1415'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='creditcard',
            name='address',
        ),
        migrations.RemoveField(
            model_name='creditcard',
            name='user',
        ),
        migrations.DeleteModel(
            name='CreditCard',
        ),
    ]
