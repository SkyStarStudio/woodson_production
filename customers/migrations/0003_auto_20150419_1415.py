# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('customers', '0002_auto_20150418_0301'),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('alias', models.CharField(max_length=20, null=True, blank=True)),
                ('full_name', models.CharField(max_length=40)),
                ('street', models.CharField(max_length=255)),
                ('zipcode', models.CharField(max_length=5)),
                ('city', models.CharField(max_length=30)),
                ('country', models.CharField(max_length=30)),
                ('state', models.CharField(max_length=30)),
                ('phone', models.CharField(max_length=15)),
                ('address_type', models.CharField(max_length=4, choices=[(b'SHIP', b'Shipment Address'), (b'BILL', b'Billing Address')])),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='CreditCard',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('card_number', models.CharField(max_length=16)),
                ('cvv', models.CharField(max_length=3)),
                ('expire_month', models.CharField(max_length=2)),
                ('expire_year', models.CharField(max_length=2)),
                ('address', models.ForeignKey(to='customers.Address')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='profile',
            name='braintree_customer',
            field=models.BooleanField(default=False),
        ),
    ]
