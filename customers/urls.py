# coding=UTF-8
from django.conf.urls import url, patterns
from . import views

urlpatterns = [
	# 注册相关
	url(r'^register/', views.register),
	url(r'^activate/', views.activate),
	url(r'^waiting_activation/', views.waiting_activation),
	url(r'^send_activation/', views.send_activation),
	# 登录相关
	url(r'^login/', views.login_user),
	url(r'^logout/', views.logout),
	url(r'^forgot/', views.forget_password),
]