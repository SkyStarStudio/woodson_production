# coding=UTF-8
import hashlib, random, re
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, HttpRequest
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.utils import six
from .forms import RegisterForm, ResendActivationEmailForm, LoginForm, ForgetPasswordForm
from .models import Profile


SHA1_RE = re.compile('^[a-f0-9]{40}$')
def activate_user(activation_key):
	"""
	对 activation_key 进行验证
	@activation_key: 验证字符串
	@return: {'status': (fail, success), 'user': 返回经过验证的User对象}
	"""
	if SHA1_RE.search(activation_key):
		profile = Profile.objects.get(activation_key=activation_key)
		user = profile.user
		user.is_active = True
		user.save()

		return {'status': 'success', 'user': user}
	else:
		return {'status': 'fail', 'user': None}

def create_activation_key(user):
	"""
	是用 SHA-1加密算法,对user的 pk 和一个 random 的 salt 进行加密
	"""
	salt = hashlib.sha1(six.text_type(random.random()).encode('ascii')).hexdigest()[:5]
	salt = salt.encode('ascii')
	user_pk = str(user.pk)
	if isinstance(user_pk, six.text_type):
		user_pk = user_pk.encode('utf-8')
	activation_key = hashlib.sha1(salt+user_pk).hexdigest()
	return activation_key


def register(request):
	"""
	处理用户注册的请求,根据RegisterForm
	1. 从 form 读取数据, 进行基本的 validation, 比如两次输入的 password 是否匹配, email地址的格式是否正确
	2. 创建 User对象,将username替换为email, 同时将用户的is_active设置为false
	3. 根据创建出来的User对象, 生成activation_key,并发送邮件邀请注册
	4. 同时生成对应的 Profile 对象.
	5. 跳转页面到等待激活的提示页面.
	"""
	if request.method == 'POST':
		form = RegisterForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			password_check = form.cleaned_data['password_check']

			# 检查是否已经注册过,如果没有注册就创建新的User和对应的Profile.
			# 此处使用filter而非get的原因是: get会抛出 User.DoesNotExist的Exception,而filter不会,只会返回一个空的list
			# 而email是我们通过方法中的约束限制为unique的,所以即使找到也只会返回一个对象
			if email and not User.objects.filter(email=email):
				# 创建一个User对象
				user = User.objects.create_user(email, email, password)
				user.is_active=False
				user.save()

				activation_key = create_activation_key(user)
				print activation_key
				profile = Profile.objects.create(user=user, nickname=username, activation_key=activation_key)
				profile.save()

				# TODO: 发送激活邮件

				return HttpResponseRedirect('/account/waiting_activation/')
			else:
				print 'user has existed'
				return HttpResponse('User has existed')

		else:
			return HttpResponse('wrong request')
	else:
		register_form = RegisterForm()
		login_form = LoginForm()

	return render(request, 'register.html', {'register_form': register_form, 'login_form': login_form})

def send_activation(request):
	"""
	发送验证邮件,可以重复发送,只能用 POST 方法重复发送
	"""
	if request.method == 'POST':
		form = ResendActivationEmailForm(request.POST)
		email = form.cleaned_data['email']

		if email and User.objects.filter(email=email):
			user = User.objects.get(email=email)
			profile = user.profile
			key = profile.activation_key
		else:
			return HttpResponse('Email does not exist')

	else:
		form = ResendActivationEmailForm()
		return render(request, 'resend_activation.html', {'form': form})

def waiting_activation(request):
	print 'waiting_activation'
	return HttpResponse('success')

def activate(request):
	"""
	对用户进行验证
	用户点击的验证链接的形式应该是/account/activate/?key=<activation_key>
	"""
	key = request.GET['key']
	result = activate_user(key)
	if result['status'] == 'success':
		# 验证成功, redirect 网页链接到网站首页
		user = result['user']
		# return HttpResponseRedirect('/', {'user': user})
		return HttpResponse('success')

	else:
		# 验证失败, 提醒再次发送邮件, redirect 到再次发送邮件的页面,然后重新输入邮箱
		return HttpResponseRedirect('/account/send_activation/')

def login_user(request):
	"""
	处理登录函数
	"""
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			print (email, password)
			user = authenticate(username=email, password=password)
			if user is not None:
				if user.is_active:
					login(request, user)
					return HttpResponseRedirect('/')
				else:
					return HttpResponse('User has not activated')
			else:
				return HttpResponse('User does not exist')

	else:
		form = LoginForm()
		return render(request, 'login.html', {'form': form})

def logout_user(request):
	"""
	处理用户登出的情况
	"""
	logout(request)

def forget_password(request):
	"""
	处理用户忘记密码的方法
	"""
	if request.method == 'POST':
		form = ForgetPasswordForm(request.POST)
		if form.is_valid():
			email = form.cleaned_data['email']
			if email and User.objects.filter(email=email):
				# TODO: 发送邮件,登录修改密码
				return HttpResponse('Email has sent')
			else:
				return HttpResponse('Email has not registered')

	else:
		form = ForgetPasswordForm()
		return render(request, 'forget_password.html', {'form': form})

def change_password(request):
	"""
	用户重设密码, 分为两种情况,第一种是用户已经登录,修改密码,另外一种情况是用户忘记密码之后根据链接重设密码
	"""
	pass


def register_customer_user(username, email, password, phone):
	"""
	注册一个直接购买的User对象,以及相应的Profile对象.
	@username
	@email
	@password
	@return: User对象
	"""
	user = User.objects.create_user(email, email, password)
	key = create_activation_key(user)
	profile = Profile.objects.create(user=user, activation_key=key, phone=phone, nickname=username)
	profile.save()

	return user

def register_guest_user(username, email):
	"""
	注册一个Checkout As Guest的用户, 不使用密码
	@username
	@email
	@return: 创建的User对象
	"""
	user = User.objects.create_user(email, email, '')
	key = create_activation_key(user)
	profile = Profile.objects.create(user=user, activation_key=key)
	profile.save()

	return user