# coding=UTF-8
from django import forms


class RegisterForm(forms.Form):
	"""
	处理注册时的表单
	这里不使用ModelForm的原因是User这个表的特殊性,包括email替换 username 等方式,直接继承forms.Form可能更方便.
	@username: 用户的nickname, 并非User表中的username
	@email: 用户的email 地址,将作为 User 表中的username和 email 使用
	@password: 密码
	@password_check: 确认密码
	"""
	username = forms.CharField(label='Username', max_length=40)
	email = forms.EmailField(label='Email', widget=forms.EmailInput)
	password = forms.CharField(label='Password', max_length=20, widget=forms.PasswordInput)
	password_check = forms.CharField(label='Confirm Password', max_length=20, widget=forms.PasswordInput)

	def clean_password_check(self):
		"""
		https://docs.djangoproject.com/en/1.8/ref/forms/validation/#validating-fields-with-clean
		对password_check这个域进行验证
		@param: 这种clean_<fieldname>方法不需要任何参数,因为执行步骤是在clean()方法之后,所有的数据可以在cleaned_data中找到
		@return: 需要返回cleaned_data的数据,也就是返回password_check
		"""
		password = self.cleaned_data['password']
		password_check = self.cleaned_data['password_check']

		if password != password_check:
			raise forms.ValidationError('Password mismatch')

		return password_check


class ResendActivationEmailForm(forms.Form):
	"""
	再次发送激活邮件的表单
	@email: 用户输入的 email 地址
	"""
	email = forms.EmailField(label='Email', widget=forms.EmailInput)


class LoginForm(forms.Form):
	"""
	登录表单
	@email
	@password
	"""
	email = forms.EmailField(label='Email', widget=forms.EmailInput)
	password = forms.CharField(label='Password', widget=forms.PasswordInput)


class ForgetPasswordForm(forms.Form):
	"""
	用于用户忘记密码时,输入 email 邮箱的情况
	"""
	email = forms.EmailField(label='Email', widget=forms.EmailInput)