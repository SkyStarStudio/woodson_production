$(document).ready(function () {
  $('.accordion-tab-headers').find("[name='shipping']").find('a').addClass('is-active');
  $('.accordion-tab-contents').children("[name='shipping']").addClass('is-open').show();

  $('.accordion-tab-headers').on('click', 'li > a', function(event) {
    if (!$(this).hasClass('is-active')) {
      event.preventDefault();
      $('.accordion-tab-headers').find('.is-active').removeClass('is-active');
      $('.accordion-tab-contents').children('.is-open').removeClass('is-open').hide();

      var name = $(this).parent().attr('name');
      $(".accordion-tab-contents").children("[name='" + name + "']").toggleClass('is-open').toggle();

      $(this).addClass('is-active');
    } else {
      event.preventDefault();
    }
  });

  var next = function(event) {
    event.preventDefault();

    var cur = $('.accordion-tab-headers').find('.is-active');

    cur.removeClass('is-active');
    $('.accordion-tab-contents').children('.is-open').removeClass('is-open').hide();

    var name = cur.parent().next().attr('name');
    var next_tab = $(".accordion-tab-contents").children("[name='" + name + "']");
    $(".accordion-tab-contents").children("[name='" + name + "']").toggleClass('is-open').toggle();
    cur.parent().next().find('a').addClass('is-active');
    // next.find('a').addClass('is-active');
  };

  $("button[name='shipping']").on('click', {}, next);

  $("button[name='billing']").on('click', {}, next);
});

// window.addEventListener('load', function() {
//     function sendData() {
//         var XHR = new XMLHttpRequest();
//         var FD = new FormData();

//         var data = {};
//         data['full_name'] = $("#full_name").val();
//         data['street'] = $("#street").val();
//         data['city'] = $("#city").val();
//         data['country'] = $("#country").val();
//         data['zip'] = $("#zip").val();
//         data['state'] = $("#state").val();
//         data['phone'] = $("#phone").val();
//         data['email'] = $("#email").val();
//         data['address_id'] = 0;
//         data['same_as_shipping'] = true;
//         data['payment_method_nonce'] = FD['payment_method_nonce']
//         var order = {};
//         order['comments'] = 'test comments';
//         data['order'] = order;
//         data['csrf_token'] = csrf_token;

//         for(name in data) {
//             FD.append(name, data[name]);
//         }
//         var formData = new FormData(form);
//         console.log(formData)
//         console.log(FD);
//         XHR.open('POST', '/order/checkout/');
//         XHR.send(FD);
//     }

//     var form = document.getElementById('checkout');
//     var csrf_token = $("input:hidden").val();

//     form.addEventListener('submit', function(event) {
//         event.preventDefault();

//         sendData();
//     })
// });

