(function(){
  'use strict';

  angular.module('ecommerce.shoppingCart', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/shoppingCart', {
        templateUrl: 'shoppingCart/shoppingCart.html',
        controller: 'ShoppingCartCtrl'
      });
    }])

    .controller('ShoppingCartCtrl', [function () {

    }]);
})();