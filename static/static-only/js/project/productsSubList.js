(function(){
  'use strict';

  angular.module('ecommerce.productsSubList', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/productsSubList', {
        templateUrl: 'productsSubList/productsSubList.html',
        controller: 'ProductsSubListCtrl'
      });
    }])

    .controller('ProductsSubListCtrl', [function () {

    }]);
})();