(function(){
  'use strict';

  angular.module('ecommerce.productsList', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/productsList', {
        templateUrl: 'productsList/productsList.html',
        controller: 'ProductsListCtrl'
      });
    }])

    .controller('ProductsListCtrl', [function () {

    }]);
})();