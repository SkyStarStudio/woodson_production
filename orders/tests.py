# coding=UTF-8
from django.test import TestCase, Client
from . import views, models, utils, braintree_handler
from django.contrib.auth.models import User
from customers.models import Profile
from customers.views import register_customer_user
from cart.views import to_str_dict
from braintree.test.nonces import Nonces


class CheckoutTestCase(TestCase):
	"""
	"""
	def setUp(self):
		self.client = Client()
		self.user = register_customer_user('maid4jesus', 'maid4jesus@yahoo.com', '123456', '3236710561')
		self.shipment = {
			'full_name': 'Ruth Magin',
			'street': 'abc street',
			'city': 'Los Angeles',
			'state': 'California',
			'country': 'United States',
			'phone': '323 671 0561',
			'email': 'maid4jesus@yahoo.com',
			'zip': '12345',
		}
		self.products = to_str_dict([
			{
				'product_pk': '1',
				'quantity': 12,
				'price': '1.2',
			},
			{
				'product_pk': '2',
				'quantity': 2,
				'price': '2.56',
			}
		])

		self.guest_shipment = {
			'full_name': 'Yue Zhang',
			'street': 'abc street',
			'city': 'Los Angeles',
			'state': 'California',
			'country': 'United States',
			'phone': '323 671 0561',
			'email': 'zhangyue1208@gmail.com',
			'zip': '12345',
		}

		self.guest_shipment_no_register = {
			'full_name': 'Jason TC',
			'street': 'abc street',
			'city': 'Los Angeles',
			'state': 'California',
			'country': 'United States',
			'phone': '323 671 0561',
			'email': 'jasontc@126.com',
			'zip': '12345',
		}

	def test_checkout_as_a_guest_register(self):
		"""
		测试Checkout_as_a_guest方法
		views.checkout_as_a_guest(data)
		@data: {
			'will_register': True,
			'password',
			'password_check',
			'shipment',
			'same_as_shipping': True,
			'products',
			'payment_method_nonce',
			'order': {
				'comments'
			}
		}
		"""
		result = views.checkout_as_a_guest(data={
			'will_register': True,
			'password': '123456',
			'password_check': '123456',
			'shipment': self.guest_shipment,
			'same_as_shipping': True,
			'products': self.products,
			'payment_method_nonce': Nonces.Transactable,
			'order': {
				'comments': '',
			}
		})
		self.assertEqual(result['meta']['status'], 200)

	def test_checkout_as_a_guest_no_register(self):
		"""
		测试用户不注册使用Guest进行Checkout
		"""
		result = views.checkout_as_a_guest(data={
			'will_register': False,
			'shipment': self.guest_shipment_no_register,
			'same_as_shipping': True,
			'products': self.products,
			'payment_method_nonce': Nonces.Transactable,
			'order': {
				'comments': '',
			}
		})
		self.assertEqual(result['meta']['status'], 200)

	def test_checkout_as_a_user_with_address(self):
		"""
		测试注册用户的购买行为,使用已有地址
		views.checkout_as_user(data, user)
		@data: {
			'address_id'
			'same_as_shipping',
			'payment_method_nonce',
			'order': {
				'comments',
			},
			'products',
		}
		"""
		result = utils.parse_address_request_form(self.shipment, 'SHIP', self.user)
		address_id = result['data']['address'].id
		same_as_shipping = True
		payment_method_nonce = Nonces.Transactable
		order = {
			'comments': '',
		}

		data = {
			'address_id': address_id,
			'same_as_shipping': True,
			'payment_method_nonce': payment_method_nonce,
			'order': order,
			'products': self.products,
		}
		result = views.checkout_as_user(data, self.user)
		self.assertEqual(result['meta']['status'], 200)


	def test_checkout_as_a_user_with_new_address(self):
		"""
		测试注册用户的购买行为,使用新地址
		views.checkout_as_user(data, user)
		@data: {
			'address_id': 0,
			'shipment': {
				'full_name',
				'street',
				'city',
				'state',
				'country',
				'phone',
				'email',
				'zip'
			},
			'same_as_shipping': False,
			'billing',
			'payment_method_nonce',
			'order': {
				'comments',
			},
			'products',
		}
		"""
		address_id = 0
		same_as_shipping = False
		payment_method_nonce = Nonces.Transactable
		order = {
			'comments': '',
		}
		data = {
			'address_id': address_id,
			'shipment': self.shipment,
			'billing': self.shipment,
			'same_as_shipping': same_as_shipping,
			'products': self.products,
			'payment_method_nonce': payment_method_nonce,
			'order': order,
		}
		result = views.checkout_as_user(data, self.user)
		self.assertEqual(result['meta']['status'], 200)

	def test_parse_address_with_user(self):
		"""
		测试注册用户的解析地址
		"""
		result = utils.parse_address_request_form(self.shipment, 'SHIP', self.user)
		self.assertEqual(result['meta']['status'], 200)

	def test_create_order(self):
		"""
		创建订单
		"""
		order_info = {
			'products': self.products,
			'tax': 10.12,
			'shipment': {
				'address_id': '1',
				'fee': '10',
			},
			'comments': '',
		}
		result = utils.create_order(order_info, self.user)
		self.assertEqual(result['meta']['status'], 200)

	def test_create_transaction(self):
		"""
		测试创建一个braintree的transaction,暂时不submit
		braintree_handler.create_transaction(order, payment_method_nonce, user)
		"""
		order_info = {
			'products': self.products,
			'tax': 10.12,
			'shipment': {
				'address_id': '1',
				'fee': '10',
			},
			'comments': '',
		}
		result = utils.create_order(order_info, self.user)
		self.assertEqual(result['meta']['status'], 200)

		order = result['data']['order']
		payment_method_nonce = Nonces.Transactable

		result = braintree_handler.create_transaction(order, payment_method_nonce, self.user)
		self.assertEqual(result['meta']['status'], 200)

	def test_submit_for_settlement(self):
		"""
		测试提交transaction
		"""
		order_info = {
			'products': self.products,
			'tax': 10.12,
			'shipment': {
				'address_id': '1',
				'fee': '10',
			},
			'comments': '',
		}
		result = utils.create_order(order_info, self.user)
		order = result['data']['order']
		payment_method_nonce = Nonces.Transactable

		result = braintree_handler.create_transaction(order, payment_method_nonce, self.user)
		transaction = result['data']['transaction']
		result = braintree_handler.submit_transaction(transaction)
		self.assertTrue(result.is_success)