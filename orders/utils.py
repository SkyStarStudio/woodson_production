# coding=UTF-8
from customers.models import Address
from .models import Order, Payment
from utilities.format import json_response


def parse_address_request_form(address_info, address_type, user):
	"""
	从request的POST数据中,将address解析出来
	@address_info
	@type: 地址的类型
	@user: User对象作为外键储存在Address表中
	@return: {'meta': {'status', 'message'}, 'data'}
	"""
	address = Address.objects.create(
				full_name=address_info['full_name'],
				street=address_info['street'],
				zipcode=address_info['zip'],
				city=address_info['city'],
				country=address_info['country'],
				state=address_info['state'],
				phone=address_info['phone'],
				address_type=address_type,
				user=user
			)
	address.save()
	if address.id is not None:
		return json_response(200, 'Create Address Success', {'address': address})
	else:
		return json_response(500, 'Create Address Failed')

def create_order(order_info, user):
	"""
	创建一个订单
	@user: 创建订单的外键
	@order_info: {
		'comments',
		'tax',
		'shipment': {'address_id', 'fee'},
		'products': [{'product_pk', 'quantity', 'price'}, ...],
	}
	由于hstore不能使用int,要使用str格式
	"""
	total_price = 0
	for product in order_info['products']:
		total_price += float(product['quantity']) * float(product['price'])

	total_price += float(order_info['tax'])
	total_price += float(order_info['shipment']['fee'])

	order = Order.objects.create(
		user=user,
		comments=order_info['comments'],
		shipment=order_info['shipment'],
		total_price=total_price,
		products=order_info['products'],
		tax=order_info['tax'],
	)
	order.order_id = order.generate_order_id()
	order.save()
	if order.id is not None:
		return json_response(200, 'Create Order Successfully', {'order': order})
	else:
		return json_response(500, 'Create Order Failed')