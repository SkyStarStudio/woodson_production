# coding=UTF-8
from django import forms


class ShippingForm(forms.Form):
	"""
	快递清单的表单
	@full_name
	@street_address
	@zip
	@city
	@country
	@state
	@phone
	@email
	"""
	full_name = forms.CharField(label='Full Name', max_length=20)
	street_address = forms.CharField(label='Stree Address', max_length=100)



class BillingForm(forms.Form):
	"""
	Billing 的表单
	@method
	@credit_card_number
	@cvv_code
	@expire_data
	@billing_address
	"""
	pass


class CheckoutForm(forms.Form):
	"""
	最后 Review & Submit的表单
	@password
	@password_check
	"""
	pass