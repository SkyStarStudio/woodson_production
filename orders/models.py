# coding=UTF-8
import arrow
from django.db import models
from django.contrib.postgres.fields import HStoreField, ArrayField
from django.contrib.auth.models import User


ORDER_STATUS_CHOICES = (
	('IN', 'Initial'),
	('WP', 'Waiting Payment'),
	('PA', 'Payed'),
	('HS', 'Handling & Shipping'),
	('FI', 'Finish Shipping'),
	('FA', 'Failed'),
	('WR', 'Waiting Review'),
	('CL', 'Closed'),
)
class Order(models.Model):
	"""
	Order Model
	@order_id: 与products.Product的product_id,使用字母+数字的方式生成
	@shipment: method, tracking_id, fee, address(暂时考虑采用储存dict的形式,之后会转为model)
	@status
	@user
	@comments
	@total_price
	@tax
	@products
	@error_message 一旦订单出错, 将错误信息储存到这个域中
	"""
	order_id = models.CharField(max_length=255, blank=True, null=True, unique=True)
	status = models.CharField(choices=ORDER_STATUS_CHOICES, max_length=2, default='IN')
	user = models.ForeignKey(User)
	comments = models.TextField(null=True, blank=True)
	total_price = models.DecimalField(max_digits=10, decimal_places=2)
	tax = models.DecimalField(max_digits=10, decimal_places=2)
	shipment = HStoreField(null=True, blank=True)
	products = ArrayField(HStoreField())
	updated_date = models.DateTimeField(auto_now=True)
	created_date = models.DateTimeField(auto_now_add=True)
	error_message = models.CharField(max_length=1000, null=True, blank=True)

	def generate_order_id(self):
		"""
		生成随机的order_id: 当前的 timestamp_self.id
		"""
		utc = arrow.now()
		return '_'.join([str(utc.timestamp), str(self.id)])

	def __str__(self):
		return self.user.username + ': ' + str(len(self.products)) + ' products-$' + str(self.total_price)


PAYMENT_STATUS_CHOICES = (
	('IN', 'Initial'),
	('WP', 'Waiting Payment'),
	('SU', 'Success'),
	('FA', 'Failed'),
)
PAYMENT_METHOD_CHOICES = (
	('CREDIT_CARD', 'Credit Card'),
	('PAYPAL', 'PayPal'),
)
class Payment(models.Model):
	"""
	@order
	@method
	@status
	@error_message
	"""
	order = models.ForeignKey(Order)
	method = models.CharField(max_length=12, choices=PAYMENT_METHOD_CHOICES, null=True, blank=True)
	status = models.CharField(max_length=2, choices=PAYMENT_STATUS_CHOICES, default='IN')
	error_message = models.CharField(max_length=1000, blank=True, null=True)

	def __str__(self):
		return self.order.order_id + ': ' + self.status