# coding=UTF-8
#
# 用来处理braintree支付相关的逻辑
#
from braintree import Transaction, Customer, ClientToken, Configuration, Environment
from customers.models import Profile
from django.contrib.auth.models import User
from utilities.format import json_response


# Sandbox API Keys
API_KEYS = {
	'merchant_id':	'qymc7fr9ct6xbd9x',
	'public_key': '8d9g5rg8jsm4r2qm',
	'private_key': '7d88960a48e944ee76f511ae4f25e1ff',
	'cse_key': 'MIIBCgKCAQEAttvzhxtNmLlSJ0nPLwrwOrdtvU8sYVnVG3LYQrgMOI1WOY3J7Crsr1/LHrAVMrIr/IakrU9ChHPVHx33Wyg5czkx3QGEvMO2mme7Kcn2isQbTtowoc05c9qabuRTQR05BMnmUDb1bjZjdUaWXhqgSJgrycBGh1fdc5K1e048J8kaAZPPstntwGdg0CwJy45gNHGotjK02hTnEDDS4qdc5IWcSvPziusP8WG69PIYxqBgFyL9A3DOI25etr6S9l6wIRC63F12dmkqG89PsLIOKelzAHBiEp4B8nlcOsMA+BRG0QZ1ZKNLqkxf883QpE929wwZIeDY8QaiAAcCrbUosQIDAQAB',
}

Configuration.configure(
	Environment.Sandbox,
	API_KEYS['merchant_id'],
	API_KEYS['public_key'],
	API_KEYS['private_key']
)


def create_customer(user=None):
	"""
	创建一个braintree.Customer对象
	@user: Braintree支持使用自己的 ID 作为客户 ID, 所以这里使用User的 id 作为customer_id来创建用户,不使用email的原因是braintree不支持使用@符号
	@return: 返回customer_id
	"""
	pass


def get_client_token(user=None):
	"""
	获取braintree.customer所属id的client_token,可以直接获取之前的payment
	@user: User对象
	@return: client_token
	"""
	if user:
		profile = user.profile
		if profile.braintree_customer:
			return ClientToken.generate({
				'customer_id': user.id
			})

	return ClientToken.generate()

def create_transaction(order, payment_method_nonce, user):
	"""
	创建一个transaction进行支付
	@order: Order对象
	@payment_method_nonce: 前端 JS 返回的payment_method_nonce
	@user:  发送request的 User 对象
	@return: Transaction对象
	"""
	result = None
	profile = user.profile
	result = Transaction.sale({
		'amount': str(order.total_price),
		'payment_method_nonce': payment_method_nonce,
		'order_id': order.order_id,
		'options': {
			'submit_for_settlement': False
		}
	})
	if result.is_success:
		profile.braintree_customer = True
		profile.save()
		return json_response(200, 'Create transaction sale successfully', {'transaction': result.transaction})
	else:
		return json_response(500, 'Create transaction sale failed')

def submit_transaction(transaction):
	"""
	执行transaction
	@transaction: transaction对象
	"""
	result = Transaction.submit_for_settlement(transaction.id)
	return result