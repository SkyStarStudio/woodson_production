# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields
from django.conf import settings
import django.contrib.postgres.fields.hstore


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order_id', models.CharField(max_length=255, null=True, blank=True)),
                ('status', models.CharField(default=b'IN', max_length=2, choices=[(b'IN', b'Initial'), (b'WP', b'Waiting Payment'), (b'PA', b'Payed'), (b'HS', b'Handling & Shipping'), (b'FI', b'Finish Shipping'), (b'FA', b'Failed'), (b'WR', b'Waiting Review'), (b'CL', b'Closed')])),
                ('comments', models.TextField()),
                ('total_price', models.DecimalField(max_digits=10, decimal_places=2)),
                ('tax', models.DecimalField(max_digits=10, decimal_places=2)),
                ('shipment', django.contrib.postgres.fields.hstore.HStoreField(null=True, blank=True)),
                ('products', django.contrib.postgres.fields.ArrayField(base_field=django.contrib.postgres.fields.hstore.HStoreField(), size=None)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('error_message', models.CharField(max_length=1000, null=True, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('method', models.CharField(blank=True, max_length=7, null=True, choices=[(b'VISA', b'Visa Card'), (b'MASTER', b'Master Card'), (b'PAYPAL', b'PayPal')])),
                ('status', models.CharField(default=b'IN', max_length=2, choices=[(b'IN', b'Initial'), (b'WP', b'Waiting Payment'), (b'SU', b'Success'), (b'FA', b'Failed')])),
                ('error_message', models.CharField(max_length=1000)),
                ('order', models.ForeignKey(to='orders.Order')),
            ],
        ),
    ]
