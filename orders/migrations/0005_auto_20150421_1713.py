# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0004_auto_20150419_1403'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='error_message',
            field=models.CharField(max_length=1000, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='payment',
            name='method',
            field=models.CharField(blank=True, max_length=12, null=True, choices=[(b'CREDIT_CARD', b'Credit Card'), (b'PAYPAL', b'PayPal')]),
        ),
    ]
