# coding=UTF-8
#
# 订单逻辑进行拆分:
# 1. 注册用户和 Guest 用户进行拆分, 也方便测试
# 	checkout_as_user, checkout_as_guest进行区分
# 2. 将注册用户的新地址和选择已有地址进行拆分
# 	parse_address_information进行分解
# 3. 将collect-order-payment逻辑进行拆分.
#	collect_inforamtion, create_order, create_payment
#
# Checkout 流程:
## 登录用户:
## 1. 确认Shipping Address信息
## 1.1 输入新的地址, 将表单的所有数据进行传递
## 1.2 选择一个已经有的地址,只传递地址的pk即可
#
## 2. 确认Billing 信息: 使用Braintree进行支付系统的管理,需要记录无论是信用卡支付还是Paypal支付时, Braintree 生成的nonce记录下来
#
## 3. 展示信息,询问注册, 提交订单
#
## Guest用户:
## 1. 确认Shipping Address和Billing Address信息,都需要输入新的
## 2. 确认提交订单支付
#
from django.shortcuts import render
from django.http import HttpResponseBadRequest, HttpResponse, JsonResponse, HttpResponseRedirect
from .models import Order, Payment
from customers.models import Address
from customers.views import register_customer_user, register_guest_user
from cart.views import checkout as cart_checkout
from .utils import parse_address_request_form, create_order
from .braintree_handler import get_client_token, create_transaction, submit_transaction
from utilities.format import json_response
from products.models import Product, Review


def checkout(request):
	"""
	CheckOut 时仍然分为GET和 POST 两种方式去考虑
	1. GET: 生成 checkout 的页面(注册用户和Guest用户需要分开考虑)
	2. POST:
		2.1 collect_inforamtion
		2.2 create_order
		2.3 create_payment
	"""
	# 获取request.session中的User对象,如果不存在,就按照Checkout As A Guest的流程来处理
	user = request.user

	if request.method == 'GET':
		# 需要根据用户是否已经登录来进行 render
		# if user:
		# 	# 如果已经登录,需要提供用户的shipping address和billing 信息
		# 	shipping_address_list = Address.objects.filter(user=user, address_type='SHIP')
		# 	billing_address_list = Address.objects.filter(user=user, address_type='BILL')

		# 	return render(request, 'checkout.html', {
		# 		'shipping_address_list': shipping_address_list,
		# 		'billing_address_list': billing_address_list,
		# 		'client_token': get_client_token(user),
		# 	})
		# else:
			return render(request, 'checkout.html', {'client_token': get_client_token()})

	else:
		# 这里因为情况比较多变,所以不使用 Django.forms, 手动验证
		products = cart_checkout(request)
		origin_data = request.POST
		data = dict()
		data = origin_data.copy()
		data['products'] = products
		data['shipment'] = {
			'full_name': 'Ruth Magin',
			'street': 'abc street',
			'city': 'Los Angeles',
			'state': 'California',
			'country': 'United States',
			'phone': '323 671 0561',
			'email': 'maid4jesus@yahoo.com',
			'zip': '12345',
		}
		data['same_as_shipping'] = True
		data['address_id'] = 0
		data['order'] = {
			'comments': 'test comments',
		}

		if user:
			result = checkout_as_user(data, user)
		else:
			result = checkout_as_a_guest(data)

		if result['meta']['status'] == 200:
			order = result['data']['order']
			user = result['data']['user']
			add_product_to_waiting_review(products, order, user)

			return HttpResponseRedirect('/')
		else:
			return HttpResponse(request['meta']['message'])


def checkout_as_user(data, user):
	"""
	注册用户进行collect_information, create_order, create_payment
	@data: request.POST
	@user: 注册用户对象
	@return: {'meta': {'status': 状态码, 'message':}, 'data':}
	"""
	# 解析地址
	address_id = data['address_id']
	shipping_address = None
	if address_id == 0:
		# 新地址
		address_info = {
			'full_name': data['shipment']['full_name'],
			'street': data['shipment']['street'],
			'zip': data['shipment']['zip'],
			'city': data['shipment']['city'],
			'country': data['shipment']['country'],
			'state': data['shipment']['state'],
			'phone': data['shipment']['phone'],
		}
		result = parse_address_request_form(address_info, 'SHIP', user)
		if result['meta']['status'] == 200:
			shipping_address = result['data']['address']
		else:
			return {
				'meta': {
					'status': 500,
					'message': 'Create address failed'
				},
				'data': {}
			}

	else:
		# 直接读取地址对象
		shipping_address = Address.objects.get(id=address_id)

	# 解析billing address
	same_as_shipping = data['same_as_shipping']
	billing_address = None
	if not same_as_shipping:
		address_info = {
			'full_name': data['billing']['full_name'],
			'street': data['billing']['street'],
			'zip': data['billing']['zip'],
			'city': data['billing']['city'],
			'country': data['billing']['country'],
			'state': data['billing']['state'],
			'phone': data['billing']['phone'],
		}
		result = parse_address_request_form(address_info, 'BILL', user)
		if result['meta']['status'] == 200:
			billing_address = result['data']['address']
		else:
			return {
				'meta': {
					'status': 500,
					'message': 'create billing address failed'
				},
				'data': {}
			}
	else:
		# 与Shipping Address相同, 此时应该先查找相同
		billing_address = shipping_address

	# 解析payment的数据,这里主要使用braintree的nonce
	payment_method_nonce = data['payment_method_nonce']

	# 创建订单
	order_info = {
		'comments': data['order']['comments'],
		'tax': 0,
		'shipment': {
			'address_id': str(shipping_address.id),
			'fee': '10',
		},
		'products': data['products'],
	}
	order_result = create_order(order_info, user)
	order = None
	if order_result['meta']['status'] == 200:
		# 订单创建成功,进行支付环节
		order = order_result['data']['order']
	else:
		return order_result

	# 创建一个payment
	payment = Payment.objects.create(order=order)
	payment.save()

	# 创建一个transaction
	transaction_sale_result = create_transaction(order, payment_method_nonce, user)
	transaction = None
	if transaction_sale_result['meta']['status'] == 200:
		order.status = 'WP'
		order.save()
		transaction = transaction_sale_result['data']['transaction']

		if transaction.payment_instrument_type == 'PayPalAccount':
			payment.method = 'PAYPAL'
		elif transaction.payment_instrument_type == 'CreditCard':
			payment.method = 'CREDIT_CARD'

		payment.status = 'WP'
		payment.save()

	else:
		payment.status = 'FA'
		payment.error_message = 'Braintree: Create transaction failed'
		payment.save()

		order.error_message = 'Create payment failed'
		return json_response(500, order.error_message, {'order': order})

	# 确认支付
	pay_result = submit_transaction(transaction)
	if pay_result.is_success:
		# 支付成功
		order.status = 'PA'
		order.save()

		payment.status = 'SU'
		payment.save()

		return json_response(200, 'order has pay', {'order': order, 'transaction': transaction, 'user': user})
	else:
		order.status = 'FA'
		order.error_message = 'Braintree: submit for settlement failed'
		order.save()

		payment.status = 'FA'
		payment.error_message = 'Braintree: submit for settlement failed'
		payment.save()

		return json_response(500, 'order payment has failed')

def checkout_as_a_guest(data):
	"""
	作为Guest用户时的处理: 前端可以根据当前request.session的情况查看是否有user登陆,所以传过来的数据都是直接可以用的
	@data: 对应需要的数据
	{
		'shipment': {全部是shipping address表单中的信息},
		'will_register': (True, False),
		'password',
		'password_check'
		'products': 购物车中的所有商品[{}, ]
	}
	@user: 匿名账户
	"""
	shipment = data['shipment']
	user = None
	if data['will_register']:
		password = data['password']
		password_check = data['password_check']
		if password and password_check and password == password_check:
			# 先注册一个在正常的用户,此时不需要验证
			user = register_customer_user(shipment['full_name'], shipment['email'], password, shipment['phone'])
		else:
			return {'status': 'fail', 'error_message': 'password dismatch'}
	else:
		# 注册一个不使用密码的 Guest 用户
		user = register_guest_user(shipment['full_name'], shipment['email'])

	# 保存派送地址
	shipping_address = None
	result = parse_address_request_form(shipment, 'SHIP', user)
	if result['meta']['status'] == 200:
		shipping_address = result['data']['address']
	else:
		return {
			'meta': {
				'status': 500,
				'message': 'Create address failed'
			},
			'data': {}
		}
	# 解析billing address
	same_as_shipping = data['same_as_shipping']
	billing_address = None
	if not same_as_shipping:
		result = parse_address_request_form(data['billing'], 'BILL', user)
		if result['meta']['status'] == 200:
			billing_address = result['data']['address']
		else:
			return {
				'meta': {
					'status': 500,
					'message': 'create billing address failed'
				},
				'data': {}
			}
	else:
		# 与Shipping Address相同, 此时应该先查找相同
		billing_address = shipping_address

	# 解析payment的数据,这里主要使用braintree的nonce
	payment_method_nonce = data['payment_method_nonce']

	# 创建订单
	order_info = {
		'comments': data['order']['comments'],
		'tax': 0,
		'shipment': {
			'address_id': str(shipping_address.id),
			'fee': '10',
		},
		'products': data['products'],
	}
	order_result = create_order(order_info, user)
	order = None
	if order_result['meta']['status'] == 200:
		# 订单创建成功,进行支付环节
		order = order_result['data']['order']
	else:
		return order_result

	# 创建一个payment
	payment = Payment.objects.create(order=order)
	payment.save()

	# 创建一个transaction
	transaction_sale_result = create_transaction(order, payment_method_nonce, user)
	transaction = None
	if transaction_sale_result['meta']['status'] == 200:
		order.status = 'WP'
		order.save()
		transaction = transaction_sale_result['data']['transaction']

		if transaction.payment_instrument_type == 'PayPalAccount':
			payment.method = 'PAYPAL'
		elif transaction.payment_instrument_type == 'CreditCard':
			payment.method = 'CREDIT_CARD'

		payment.status = 'WP'
		payment.save()

	else:
		payment.status = 'FA'
		payment.error_message = 'Braintree: Create transaction failed'
		payment.save()

		order.error_message = 'Create payment failed'
		return json_response(500, order.error_message, {'order': order})

	# 确认支付
	pay_result = submit_transaction(transaction)
	if pay_result.is_success:
		# 支付成功
		order.status = 'PA'
		order.save()

		payment.status = 'SU'
		payment.save()

		return json_response(200, 'order has pay', {'order': order, 'transaction': transaction, 'user': user})
	else:
		order.status = 'FA'
		order.error_message = 'Braintree: submit for settlement failed'
		order.save()

		payment.status = 'FA'
		payment.error_message = 'Braintree: submit for settlement failed'
		payment.save()

		return json_response(500, 'order payment has failed')

def add_product_to_waiting_review(products, order, user):
	"""
	在支付成功后,将已经购买的商品加入到Review表中
	@products: 商品列表
	@order: 订单对象
	@user: 购买商品的人
	"""
	for product_dict in products:
		product = Product.objects.get(id=product_dict['product_pk'])
		review = Review.objects.create(order=order, user=user, product=product, rating=5, content='very good')
		review.save()