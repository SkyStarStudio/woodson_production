from django.conf.urls import include, url, patterns
from django.contrib import admin
from storefront.views import index, product_detail
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    # Examples:
    # url(r'^$', 'woodson_django.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^account/', include('customers.urls')),
    url(r'^cart/', include('cart.urls')),
    url(r'^$', index),
    url(r'^product/(?P<product_id>[0-9]+)/$', product_detail),
    url(r'^order/', include('orders.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )