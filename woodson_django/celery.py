# coding=UTF-8
from __future__ import absolute_import
from django.conf import settings
from celery import Celery
import django, os


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'woodson_django.settings')
django.setup()

app = Celery('woodson_django')

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)