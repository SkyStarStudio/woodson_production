# coding=UTF-8
from django.conf.urls import url
from . import views


urlpatterns = [
	url(r'^add/', views.add),
	url(r'^remove/', views.remove),
	url(r'^$', views.cart),
]