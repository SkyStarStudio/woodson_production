# coding=UTF-8
#
# Shopping-Cart暂时保存在session中, 以后再考虑增加到数据库中
# 其中checkout和clear函数是为了提供接口给order的方法,这样做的目的是为了以后的变动做考虑, 即使改为database-based的cart, 也不需要修改
# order 的逻辑
#
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from products.models import Product
from carton.cart import Cart
from utilities.format import json_response


def add(request):
	"""
	向购物车内增加商品
	"""
	print request.GET
	print request.POST

	cart = Cart(request.session)
	product = Product.objects.get(id=request.GET['id'])
	quantity = request.POST['quantity']
	cart.add(product, price=product.price, quantity=quantity)
	return HttpResponseRedirect('/cart/')

def remove(request):
	"""
	删除一个商品
	"""
	cart = Cart(request.session)
	product = Product.objects.get(id=request.GET['id'])
	cart.remove(product)
	return HttpResponseRedirect('/cart/')

def show(request):
	"""
	整理购物车内容
	@request: 通过session获取购物车内容
	@return: {
		'meta': {
			'status',
			'message',
		},
		'data': {
			'products': [
				{
					'product':
					'quantity':
				}
			]
		}
	}
	"""
	cart = Cart(request.session)
	products_list = list()
	for item in cart.items:
		product = Product.objects.get(id=item.product)
		products_list.append({
			'product': product,
			'quantity': product_dict['quantity'],
		})
	return json_response(200, '', {'products': product_list})

def checkout(request):
	"""
	将所有的产品提取出为一个 [{}, ...]格式的数据包
	@request: 从orders.views.checkout转过来的request, 包含session
	@return: 返回[{'product_pk', 'quantity', 'price'}, ...]的数据包
	"""
	cart = Cart(request.session)
	return map(lambda x: to_str_dict(x.to_dict()), cart.items)

def to_str_dict(product_list):
	"""
	将所有的dict中的域转为str类型
	"""
	# for product_dict in iter(product_list):
		# for key in iter(product_dict):
			# product_dict[key] = str(product_dict[key])
	product_dict = dict()
	for key in iter(product_list):
		product_dict[key] = str(product_list[key])

	return product_dict

def clear(request):
	"""
	清空商品
	"""
	cart = Cart(request.session)
	cart.clear()

def cart(request):
	"""
	显示页面,调用show()方法
	"""
	cart = Cart(request.session)
	fee = 10.00
	if cart.total == 0:
		fee = 0.00
	# cart.clear()
	# p = Product.objects.get(id=1)
	# cart.add(p, price=p.price, quantity=3)
	total = float(cart.total) + fee

	return render(request, 'cart.html', {'fee': fee, 'total': total})