# coding=UTF-8


def json_response(status, message=None, data=None):
	"""
	生成一个 JSON 格式的 response
	@status
	@message
	@data: {}
	@return: {
		'meta': {
			'status',
			'message'
		},
		'data': {}
	}
	"""
	return {
		'meta': {
			'status': status,
			'message': message,
		},
		'data': data
	}