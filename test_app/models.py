from django.db import models
from django.contrib.postgres.fields import ArrayField, HStoreField


class TestModel(models.Model):
	hstore = HStoreField()
	nest = ArrayField(HStoreField())