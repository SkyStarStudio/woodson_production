# coding=UTF-8
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404
from products.views import get_feature_products, get_newest_products, get_product_detail


def index(request):
	"""
	首页
	"""
	feature_products_result = get_feature_products()
	feature_products = None
	if feature_products_result['meta']['status'] == 200:
		feature_products = feature_products_result['data']['products']

	newest_products_result = get_newest_products()
	newest_products = None
	if newest_products_result['status'] == 'success':
		newest_products = newest_products_result['products']

	return render(request, 'index.html', {'newest_products': newest_products, 'best_products': newest_products, 'feature_products': feature_products,})


def product_detail(request, product_id):
	"""
	显示product详情
	"""
	result = get_product_detail(product_id)
	if result['status'] == 'success':
		return render(request, 'detail.html', {'product': result['product'], 'reviews': result['reviews']})