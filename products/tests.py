# coding=UTF-8
from django.test import TestCase
from django.core.exceptions import ValidationError
from .models import Product, Category, Review
from .views import get_newest_products, get_all_category_list


class ModelTestCase(TestCase):
	def setUp(self):
		Category.objects.create(
			name='Beauty & Health',
			level=1
		)

	def test_product_model(self):
		pass

	def test_category_model_create_first_level_category(self):
		"""
		测试创建一级分类对象
		"""
		instance = Category.objects.get(name='Beauty & Health')
		self.assertEqual(instance.level, 1)

	def test_category_model_create_second_level_category_without_parent(self):
		instance = Category.objects.create(name='UV Nail Lamp', level=2)
		instance.save()



class ViewTestCase(TestCase):
	def setUp(self):
		pass

	def test_get_newest_products(self):
		pass

	def test_get_all_category_list(self):
		pass