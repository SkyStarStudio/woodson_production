# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('level', models.PositiveSmallIntegerField()),
                ('parent', models.ForeignKey(related_name='parent_category', blank=True, to='products.Category', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_id', models.CharField(max_length=255, null=True, verbose_name=b'product id', blank=True)),
                ('name', models.CharField(max_length=255)),
                ('price', models.DecimalField(max_digits=5, decimal_places=2)),
                ('detail', models.TextField()),
                ('inventory', models.PositiveIntegerField()),
                ('color', models.CharField(max_length=255)),
                ('image', models.ImageField(upload_to=b'thumb')),
                ('publish', models.BooleanField(default=False, help_text=b'check this befor publish')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('category', models.ForeignKey(related_name='category', to='products.Category')),
                ('sub_category', models.ForeignKey(related_name='sub_category', to='products.Category')),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.PositiveSmallIntegerField()),
                ('content', models.CharField(max_length=1000)),
                ('product', models.ForeignKey(to='products.Product')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
