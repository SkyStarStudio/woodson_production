# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0003_auto_20150417_0722'),
        ('products', '0002_category_product_review'),
    ]

    operations = [
        migrations.AddField(
            model_name='review',
            name='order',
            field=models.ForeignKey(default=1, to='orders.Order'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='review',
            name='status',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_id',
            field=models.CharField(max_length=255, unique=True, null=True, verbose_name=b'product id', blank=True),
        ),
    ]
