# coding=UTF-8
"""
只在model 对应的 view 中储存获得相应 list 的方法,跟storefront 和 dashboard 的 url 映射相关的方法放在对应的 app 中
"""
from django.shortcuts import render
from .models import Product, Category, Review
from django.core.exceptions import ObjectDoesNotExist


def get_all_category_list(category_name=None):
	"""
	返回所有的分类列表
	@category: 一级分类的名字,默认为空,即返回所有的一级分类
	@return: {'status': (fail, success), 'categories': [category, ...], 'error_message': ''}
	"""
	if category_name is None:
		try:
			categories = Category.objects.filter(level=1).order_by('name')
		except ObjectDoesNotExist:
			return {'status': 'fail', 'error_message': 'Category does not exist'}
		else:
			return {'status': 'success', 'categories': categories}
	else:
		try:
			first_category = Category.objects.get(level=1, name=category_name)
		except ObjectDoesNotExist:
			return {'status': 'fail', 'error_message': 'Category does not exist'}

		try:
			categories = Category.objects.filter(level=2, parent=first_category).order_by('name')
		except ObjectDoesNotExist:
			return {'status': 'fail', 'error_message': 'Category does not exist'}
		else:
			return {'status': 'success', 'categories': categories}


def get_feature_products():
	"""
	获取标为feature的三件商品
	"""
	products = Product.objects.filter(publish=True).order_by('created_date')
	return {
		'meta': {
			'status': 200,
			'message': '',
		},
		'data': {
			'products': products[:3],
		}
	}


def get_newest_products(category_name=None, sub_category_name=None, home_page=True):
	"""
	获取最新上架的产品
	@category: 一级category的 name
	@sub_category: 二级 Category 的 name
	@home_page: 首页只展示5个商品,但是在product列表页要分开显示
	@return {'status': (fail, success), 'error_message', 'products'}
	"""
	if category_name is None and sub_category_name is not None:
		# 指定了二级 Category, 并没有指定一级 Category, 非法请求
		return {'status': 'fail', 'error_message': 'Illegal Query'}
	else:
		if home_page:
			products = Product.objects.filter(publish=True).order_by('created_date')
			if products.count() >= 5:
				return {'status': 'success', 'products': products[:5]}
			else:
				return {'status': 'success', 'products': products}
		else:
			if category_name is None and sub_category_name is None:
				# products list页面
				# 首先获取所有的一级Category, 然后获取最新的products
				categories_result = get_all_category_list()
				if categories_result['status'] == 'success':
					products_list = list()
					for category in categories_result['categories']:
						products = Product.objects.filter(publish=True, category=category).order_by('created_date')
						if products.count() >= 5:
							products_list.append({'category': category, 'products': products[:4]})
						else:
							products_list.append({'category': category, 'products': products})
					return {'status': 'success', 'products': products_list}
				else:
					return {'status': 'fail', 'error_message': 'Can not retreive category list'}

			elif category_name is not None:
				# 用于一级产品列表,指定了一级Category
				sub_category_list = get_all_category_list(category_name)
				if sub_category_list['status'] == 'success':
					products_list = list()
					for category in sub_category_list['categories']:
						products = Product.objects.filter(publish=True, sub_category=category).order_by('created_date')
						if products.count() >= 4:
							products_list.append({'category': category, 'products': products[:4]})
						else:
							products_list.append({'category': category, 'products': products})
					return {'status': 'success', 'products': products_list}
				else:
					return {'status': 'fail', 'error_message': 'Category is not exists'}


def get_product_list(sub_category_name):
	"""
	获取详细的产品列表
	@sub_category_name: 二级产品分类名称
	@return: {'status': (success, fail), 'products', 'error_message'}
	"""
	try:
		sub_category = Category.objects.get(name=sub_category_name)
	except ObjectDoesNotExist:
		return {'status': 'fail', 'error_message': 'Category does not exists'}

	products = Product.objects.filter(sub_category=sub_category)
	return {'status': 'success', 'products': products}


def get_product_detail(product_id):
	"""
	获取产品所有详情
	@product_id: 产品id
	@return: 产品所有详情 {'status': (success, fail), 'product', 'error_message', 'reviews'}
	"""
	try:
		product = Product.objects.get(id=product_id)
	except ObjectDoesNotExist:
		return {'status': 'fail', 'error_message': 'Product does not exists'}
	else:
		# 获取此产品所有的 reviews
		try:
			print 'product: ', product
			reviews = Review.objects.filter(product=product)
			print 'reviews: ', reviews
		except ObjectDoesNotExist:
			reviews = []

		return {'status': 'success', 'product': product, 'reviews': reviews}