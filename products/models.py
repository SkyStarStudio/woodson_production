# coding=UTF-8
from django.db import models
from django.contrib.postgres.fields import ArrayField, HStoreField
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from PIL import Image
from django.utils.timezone import now
from orders.models import Order
import arrow


class Category(models.Model):
	"""
	Category Model
	@name
	@level
	@parent: 指向自己的外键,当 level=2时,必须存在,所以要在clean(self)方法中做一下检测
	"""
	name = models.CharField(max_length=255)
	level = models.PositiveSmallIntegerField()
	parent = models.ForeignKey('self', related_name='parent_category', null=True, blank=True)

	def clean(self):
		"""
		当level==2时, parent 不能为空
		"""
		if self.level == 2 and self.parent is None:
			raise ValidationError({'parent': "Secondary level's parent category could not be empty"})

		if self.level == 1 and self.parent is not None:
			raise ValidationError({'parent': "First level category should not has a parent"})

		super(Category, self).clean()

	def __str__(self):
		if not self.parent:
			return self.name
		else:
			return self.parent.name + ': ' + self.name


class Product(models.Model):
	"""
	Product Model for products storing in the database
	@id: 默认自增的PK, 可以在作为外键时进行 query 使用,与下面的product_id不同.
	@product_id: 与 django 默认的自增id不同,应该采用字母+数字的编码方式
	@name
	@price
	@category
	@subcategory
	@detail: 直接保存带有 HTML 的description
	@inventory
	@color
	@image
	"""
	product_id = models.CharField(
		'product id',
		max_length=255,
		blank=True,
		null=True,
		unique=True
	)
	name = models.CharField(max_length=255)
	price = models.DecimalField(
		max_digits=5,
		decimal_places=2
	)
	detail = models.TextField()
	inventory = models.PositiveIntegerField()
	color = models.CharField(max_length=255)
	image = models.ImageField(upload_to='thumb')
	category = models.ForeignKey(
		Category,
		related_name='category'
	)
	sub_category = models.ForeignKey(
		Category,
		related_name='sub_category'
	)
	publish = models.BooleanField(default=False, help_text='check this befor publish')
	created_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.category.name + '-' + self.sub_category.name + ': ' + self.name

	def generate_product_id(self, params):
		"""
		用于生成 product_id, 采用的算法是category的 name 取前三个字母大写, name 和 pid 进行_拼接的方式
		"""
		product_name = '_'.join(map(lambda x: x[:3].upper(), params))
		utc = arrow.now()
		return product_name + '_' + str(utc.timestamp)

	def save(self, *args, **kwargs):
		"""
		设置 thumbnail和 product_id 的相关逻辑
		"""
		# 首先将图片保存,然后在 resize
		self.product_id = self.generate_product_id([self.category.name, self.sub_category.name, self.name])
		super(Product, self).save(*args, **kwargs)

		img = Image.open(self.image)
		size = (500, 500)
		image = img.resize(size, Image.ANTIALIAS)
		image.save(self.image.path)


class Review(models.Model):
	order = models.ForeignKey(Order)
	product = models.ForeignKey(Product)
	user = models.ForeignKey(User)
	rating = models.PositiveSmallIntegerField(null=True, blank=True)
	content = models.CharField(max_length=1000, null=True)
	status = models.BooleanField(default=False)

	def __str__(self):
		return self.product.name + '-' + self.user.username + ': ' + str(self.rating)