# 安装及配置环境
## 要求

1. 确保已经成功安装postgresql数据库,版本应在9.3以上,以便使用 hstore的数据类型.
2. django >= 1.8
3. python >= 2.7
4. pip

## 配置

1. 安装postgresql, 在 Mac OS X 上,可以直接安装postgres.app, 设置默认开机启动即可, 监听的端口是5432, 用户名和密码为localhost的本机用户名和密码.
2. 安装requirements.txt 中的第三方 packages, 保证 psycopg2的版本大于2.5, 这样才能使用django 1.8版本中对于postgresql数据库的最新支持,即 HStoreField 和 ArrayField 两种很方便的 Field.
3. 安装完所有的packages之后,需要设置postgres.app的对应 command line 的 path. 添加到对应的 profile 中.
4. 为服务创建数据库,`createdb woodson`.
5. 初始化 django 项目, `python manage.py migrate`,初始化所有的 model.`python manage.py collectstatic`进行静态文件的收集.
6. 最后`python manage.py test`对项目进行测试, 只有在测试通过的基础上才能继续进行开发.

