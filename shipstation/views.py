# coding=UTF-8
# 进行与ShipStation有关的对接工作,包括订单的添加和一些数据的读取
from django.shortcuts import render
import request
from base64 import b64encode


SHIP_STATION_APIS = {
	'key': '1e09b423260f405f91bded56e13feb2f',
	'secret': '652d6d70b1f74d31bdf0ff63c61b29a6',
}
SHIP_STATION_API_END_POINT = 'https://ss.shipstation.com/'
def generate_authorization():
	"""
	对shipstation的api的key和secret进行加密
	1.将key和secret组合成为'key:secret'格式的字符串
	2.进行base64编码
	3.组成'Basic <encoded_string>'的形式
	@return: 已经拼接加密完成的 Authorization 字符
	"""
	combine_api_string = ':'.join(SHIP_STATION_APIS.values())
	return ' '.join(['Basic', b64encode(combine_api_string)])